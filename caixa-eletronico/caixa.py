def msg_erro(valor_sacado):
    msg_erro = []
    
    if valor_sacado < 0:
        msg_erro.append('Informe um valor positivo')
    if valor_sacado < 10:
        msg_erro.append('Saque um valor acima de 10 reais')
    if valor_sacado % 10 != 0:
        msg_erro.append('Informe um valor válido para o saque')

    return msg_erro

def caixa(valor_sacado):
    if len(msg_erro(valor_sacado)) > 0:
        return msg_erro(valor_sacado)[0]

    notas_disponiveis = [100, 50, 20, 10]
    notas_retornadas = []

    for index, nota in enumerate(notas_disponiveis):
        while valor_sacado != 0 and valor_sacado >= notas_disponiveis[index]:
            notas_retornadas.append(notas_disponiveis[index])
            valor_sacado -= notas_disponiveis[index]
    
    return notas_retornadas
