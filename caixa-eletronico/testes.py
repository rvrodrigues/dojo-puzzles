from caixa import caixa
import types

def testa_funcao():
    assert isinstance(caixa, types.FunctionType)

def deve_retornar_erro_se_negativo():
    assert caixa(-10) == 'Informe um valor positivo'

def deve_retornar_erro_se_valor_menor_que_10():
    assert caixa(9) == 'Saque um valor acima de 10 reais'

def deve_retornar_erro_se_valor_nao_for_divisivel_por_10():
    assert (caixa(11)) == 'Informe um valor válido para o saque'

def deve_retornar_lista_com_um_item():
    assert len(caixa(10)) == 1

def deve_retornar_uma_nota_de_dez():
    cash = caixa(10)
    assert len(cash) == 1 and cash.count(10) == 1

def deve_retornar_nota_de_20():
    assert caixa(20) == [20]

def deve_retornar_nota_de_20_e_nota_de_10():
    assert caixa(30) == [20, 10]

def deve_retornar_nota_de_50_e_nota_de_10():
    assert caixa(60) == [50, 10]

def deve_retornar_nota_de_50_de_20_e_de_10():
    assert caixa(80) == [50, 20, 10]

def deve_retornar_nota_de_100_e_de_20():
    assert caixa(120) == [100, 20]

def deve_retornar_3_notas():
    assert caixa(170) == [100, 50, 20]

def deve_retornar_2_notas_de_100():
    assert caixa(200) == [100, 100]

def deve_retornar_10_notas_de_1000():
    cash = caixa(1000)
    assert len(cash) == 10 and cash.count(100) == 10

def deve_retornar_18_notas():
    cash = caixa(1590)
    assert len(cash) == 18 and cash.count(100) == 15 and cash.count(50) == 1 and cash.count(20) == 2
